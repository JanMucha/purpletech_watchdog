﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetaQuotes;

namespace Watchdog_MT5
{
    class Program
    {
        static void Main(string[] args)
        {
            Watchdog watchdog = new Watchdog();        
            Console.WriteLine("Application start...");
            watchdog.InitWatchDog(args);   
        }
    }
}
