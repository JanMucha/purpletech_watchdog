﻿using MetaQuotes.MT5CommonAPI;
using MetaQuotes.MT5ManagerAPI;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Watchdog_MT5
{
    public class ManagerSink : CIMTManagerSink
    {
        // Init Logger
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        
        // Constructor with automatic registration of new Sink Object
        public ManagerSink()
        {
            MTRetCode res = MTRetCode.MT_RET_ERROR;

            if ((res = RegisterSink()) != MTRetCode.MT_RET_OK)
            {
                _logger.Error(" CIMTManagerSink.RegisterSink failed - {0}", res);
            }
            else { _logger.Info(" ManagerSink was successfully registered - {0}", res); }
        }

        // Method which is called when Server is disconnected
        public override void OnDisconnect()
        {
            _logger.Info("SINK: Server disconnection.");            
        }

        // Method which is called when Server is connected
        public override void OnConnect()
        {
            _logger.Info("SINK: Server connection.");
        }    
    }
}
