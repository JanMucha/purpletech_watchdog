﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Watchdog_MT5
{
    public class DealObject
    {
        public ulong DealID { get; private set; }
        public double Profit { get; private set; }
        public ulong Volume { get; private set; }
        public long TimeMsc { get; private set; }
        public string Symbol { get; private set; }
        public ulong Login { get; private set; }

        // This class serve to store the information about the deal
        public DealObject(ulong dealID, double profit, ulong volume, long timemsc, string symbol, ulong login)
        {            
            DealID = dealID;
            Profit = profit;
            Volume = volume;
            TimeMsc = timemsc;
            Symbol = symbol;
            Login = login;
        }
    }
}
