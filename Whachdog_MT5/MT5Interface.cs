﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using MetaQuotes.MT5CommonAPI;
using MetaQuotes.MT5ManagerAPI;
using System.Threading;

namespace Watchdog_MT5
{
    public class MT5Interface
    {
        // Init Logger
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        // MT5 manager
        public CIMTManagerAPI mt5Manager = null;


        // Load Manager API and create an Instance
        public MTRetCode MTInitialize()
        {
            _logger.Debug(" ManagerAPI initialization start");

            MTRetCode res = MTRetCode.MT_RET_ERROR;
            //--- Initialize the factory
            if ((res = SMTManagerAPIFactory.Initialize(@"..\..\..\API\")) != MTRetCode.MT_RET_OK)
            {
                _logger.Error(" SMTManagerAPIFactory.Initialize failed - {0}", res);
                return (res);
            }
            //--- Receive the API version
            uint version = 0;
            if ((res = SMTManagerAPIFactory.GetVersion(out version)) != MTRetCode.MT_RET_OK)
            {
                _logger.Error("SMTManagerAPIFactory.GetVersion failed - {0}", res);
                return (res);
            }
            //--- Compare the obtained version with the library one
            if (version != SMTManagerAPIFactory.ManagerAPIVersion)
            {
                _logger.Debug(" Version of the library - {0}, received version {1}", SMTManagerAPIFactory.ManagerAPIVersion, version);
                _logger.Error(" Manager API version mismatch - { 0}!={ 1}", version, SMTManagerAPIFactory.ManagerAPIVersion);
                return (MTRetCode.MT_RET_ERROR);
            }
            //--- Create an instance
            mt5Manager = SMTManagerAPIFactory.CreateManager(SMTManagerAPIFactory.ManagerAPIVersion, out res);
            if (res != MTRetCode.MT_RET_OK)
            {
                _logger.Error("SMTManagerAPIFactory.CreateManager failed - {0}", res);
                return (res);
            }
            //--- For some reasons, the creation method returned OK and the null pointer
            if (mt5Manager == null)
            {
                _logger.Error("SMTManagerAPIFactory.CreateManager was OK, but ManagerAPI is null");
                return (MTRetCode.MT_RET_ERR_MEM);
            }
            //--- All is well
            _logger.Debug(" ManagerAPI start was successful. ");
            _logger.Info(" Using ManagerAPI v. {0}", version);
            return (res);
        }

        // Connect to the trading server
        public MTRetCode MTConnect(string server, UInt64 login, string password, uint timeout)
        {
            MTRetCode res = MTRetCode.MT_RET_ERROR;
            //--- For some reasons, the creation method returned OK and the null pointer
            if (mt5Manager == null)
            {
                _logger.Error(" Connection to {0} failed: .NET Manager API is NULL", server);
                return (res);
            }
            //--- Now try to connect
            res = mt5Manager.Connect(server, login, password, null, CIMTManagerAPI.EnPumpModes.PUMP_MODE_FULL, timeout);
            if (res != MTRetCode.MT_RET_OK)
            {
                _logger.Error(" Connection by Managed API to {0} failed: { 1}", server, res);
                return (res);
            }
            //--- All is well
            _logger.Info(" Connected to {0}", server);
            return (res);
        }

        // Run Deal Watcher
        public void MTStartDealWatcher(uint openTime, ushort vol2balance)
        {
            MTRetCode res = MTRetCode.MT_RET_ERROR;

            ManagerSink mgrSink = new ManagerSink();

            if ((res = mt5Manager.Subscribe(mgrSink)) != MTRetCode.MT_RET_OK)
            {
                _logger.Error(" IMTManagerAPI.Subscribe failed - {0}", res);
                //return (res);
            }
            else { _logger.Info(" IMTManagerAPI.Subscribe was successful - {0}", res); }


            DealSink dealSink = new DealSink(openTime, vol2balance);

            if ((res = mt5Manager.DealSubscribe(dealSink)) != MTRetCode.MT_RET_OK)
            {
                _logger.Error(" IMTManagerAPI.DealSubscribe failed - {0}", res);                
            }
            else { _logger.Info(" IMTManagerAPI.DealSubscribe was successful - {0}", res); }

            // Till is not terminated
            ConsoleKeyInfo input;
            while (true)
            {
                input = Console.ReadKey();
                if (input.Key == ConsoleKey.Escape)
                {
                    break;
                    
                }
            }
        }

        // Disconnect from Server
        public void MTDisconnect()
        {
            mt5Manager.Disconnect();          
        }
        
        // Release and close MT Manager
        public MTRetCode MTShutDown()
        {
            //--- First release interface to MT5
            mt5Manager.Release();

            MTRetCode res = MTRetCode.MT_RET_ERROR;
            //--- Now shut down it
            res = SMTManagerAPIFactory.Shutdown();
            if (res != MTRetCode.MT_RET_OK)
            {
                _logger.Error(" SMTManagerAPIFactory.Shutdown failed - {0}", res);
                return (res);
            }
            //--- All is well
            _logger.Info(" ManagerAPI is shutdown.");
            return (res);
        }
    }
}
