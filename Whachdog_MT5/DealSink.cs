﻿using MetaQuotes.MT5CommonAPI;
using MetaQuotes.MT5ManagerAPI;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Watchdog_MT5
{
    public class DealSink : CIMTDealSink
    {
        // Init Logger
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        // Init List for deals
        private List<DealObject> _dealList = new List<DealObject>();

        // Properties for similar deals
        private uint    _openTime;
        private ushort  _vol2balance;       

        // Constructor
        public DealSink(uint openTime, ushort vol2balance)
        {
            // Set the similarity properties
            _openTime = openTime;
            _vol2balance = vol2balance;

            // Register new sink
            MTRetCode res = MTRetCode.MT_RET_ERROR;

            if ((res = RegisterSink()) != MTRetCode.MT_RET_OK)
            {
                _logger.Error(" CIMTDealSink.RegisterSink failed - {0}", res);
            }
            else { _logger.Info(" DealSink was successfully registered - {0}", res); }
        }

        // Override the virtual method for event handler on deal add
        public override void OnDealAdd(CIMTDeal deal)
        {   
            // Get deal action type represented by number
            uint dealAction = deal.Action();

            // Check if it is Buy(0) or Sell(1) only
            if (dealAction == 0 || dealAction == 1)
            {
                // Get properties of deal
                double profit   = deal.Profit();
                ulong volume    = deal.Volume();
                long time       = deal.TimeMsc();
                ulong dealID    = deal.PositionID();
                string symbol   = deal.Symbol();
                ulong login     = deal.Login();                

                // Log deal
                _logger.Info(" Deal #{0}, {1} {2}, Profit: {3}", dealID, dealAction == 0? "BUY" : "SELL" , symbol, profit);

                // Add deal to stack
                _dealList.Add(new DealObject(dealID, profit, volume, time, symbol, login));
                CheckDeals();
            }
        }

        // Check last deal with the rest in deal List
        private void CheckDeals()
        {
            // Get last "hot" deal
            DealObject hotDeal = _dealList.Last();
            // Check with the rest deals in stack
            foreach (DealObject deal in _dealList)
            {
                // If it last deal then stop
                if (deal.Equals(hotDeal))
                {
                    break;
                }

                // Check if currency pair is the same
                if (deal.Symbol != hotDeal.Symbol)
                {
                    continue;
                }

                // Check if open time is less than wanted                
                if ((hotDeal.TimeMsc - deal.TimeMsc) > _openTime)
                {
                    // Remove deals from list which are far more than open time delta from list
                    _dealList.Remove(deal);
                    continue;
                }

                // Check if volume to balance ratio is less than wanted
                //TODO

                // If we came here we found the suspicious deal
                _logger.Warn("SUSPICIOUS DEAL!!");
                _logger.Warn("Deal #{0} triggered match with deal #{1}", hotDeal.DealID,deal.DealID);
            }
        }
    }
}
