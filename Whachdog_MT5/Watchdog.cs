﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using NLog;
using MetaQuotes.MT5CommonAPI;

namespace Watchdog_MT5
{
    public class Watchdog : MT5Interface
    {

        // Init Logger
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        // Constructor, Check if params are correct, open connection to server and start watchdog
        public void InitWatchDog(params string[] args)
        {
            _logger.Info(" Watchdog initialization.");
            _logger.Debug(" Input params: {0}", string.Join(", ", args));            
            
            // Start WatchDog  
            if (CheckParams(args))
            {
                // Set login information
                uint openTime       = UInt32.Parse(args[0]);
                ushort vol2balance  = UInt16.Parse(args[1]);
                string server   = args[2] +":443" ;
                Console.WriteLine("Login:");
                UInt64 login = UInt64.Parse(Console.ReadLine());
                Console.WriteLine("Password:");
                string password = Console.ReadLine();                
                uint   timeout  = 10000;

                _logger.Info(" Connecting to server {0} using login {1} and timeout {2}", server, login, timeout);

                // Start WatchDog
                if (OpenConection2Server(server, login, password, timeout))
                {
                    _logger.Info(" Connection to server is open.");
                    // Start deal Watcher
                    MTStartDealWatcher(openTime, vol2balance);

                    // When watcher is interrupted (ESC pressed), close application.
                    CloseWatchDog();
                }
                else
                {
                    // Connection failed
                    CloseWatchDog();
                }
                
            }
        }

        public void CloseWatchDog()
        {
            // Close connection
            if (Shutdown())
            {
                _logger.Info(" Application shutdown");
            }
        }


        /// <summary>
        /// Check and set Input parameters for watchdog
        /// </summary>
        /// <param name="args">
        /// Input parameters in the following order:
        ///     1.) open time delta                 (1000 ms by default)
        ///     2.) trade volume-to-balance ratio   (5 % by default)
        ///     3.) servers to connect (may be multiple)
        /// </param>
        /// <returns>True/False statement when inputs are correct</returns>
        private static bool CheckParams(params string[] args)
        {
            // First check the input length (must be more than 3)
            if (args.Length < 3)
            {
                _logger.Error(" CheckParams: Not enough input arguments ({0})",args.Length);
                return false;
            }

            // Check if first two are numeric (open time; volume-balance ratio)
            if (!int.TryParse(args[0], out int value) || !int.TryParse(args[1], out value))
            {
                _logger.Error(" CheckParams: First two inputs must be numeric (open time; volume-balance ratio)! " +
                                  "Values are: {0}; {1} ", args[0], args[1]);
                return false;
            }

            // Check how many servers are selected and validate IP address
            for (int i = 2; i < args.Length; i++)
            {
                if (args[i].Count(c => c == '.') != 3 || !IPAddress.TryParse(args[i], out IPAddress address))
                {
                    _logger.Error(" CheckParams: Input number {0} is not a valid IP address!({1})", i+1,args[i]);
                    return false;
                }
            }
            // Otherwise everything is correct so return true
            _logger.Debug(" All parameters passed correctly!");
            return true;
        }


        private bool OpenConection2Server(string server, UInt64 login, string password, uint timeout)
        {
            // First Initialize the library
            if (MTInitialize() != MTRetCode.MT_RET_OK)
            {
                _logger.Warn(" ManagerAPI initialization failed.");
                return false;
            }
            // Now open the connection to server with login information
            if (MTConnect(server,login,password,timeout) != MTRetCode.MT_RET_OK)
            {
                _logger.Warn(" ManagerAPI initialization failed.");
                return false;
            }
            return true;
        }

        private bool Shutdown()
        {
            MTDisconnect();
            if (MTShutDown() != MTRetCode.MT_RET_OK)
            {
                _logger.Warn(" ManagerAPI shutdown failed.");
                return false;
            }
            return true;
        }
    }
}
